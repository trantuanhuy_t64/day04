<?php
    $name = $gender = $department = $date = $address = "" ;
    $gender_arr = array(
        0 => 'Nam',
        1 => 'Nữ'
    );    
    $departments = array(
        'MAT' => 'Khoa học máy tính',
        'KDL' => 'Khoa học vật liệu'
    );
    echo "<!DOCTYPE html>
        <html lang='vn' >
            <head>
                <meta charset='UTF-8'>
                <link rel='stylesheet' href='register.css'>
            </head>
	        <body>
            <fieldset> " ;
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $name = test_input($_POST["name"]);
                $gender = test_input(@$_POST["gender"]);
                $department = test_input($_POST["department"]);
                $date = test_input($_POST["date"]);
                $address = test_input($_POST["address"]);
                if ($name == "") {
                    echo "<font color='red'>Hãy nhập tên.</font> </br>"; 
                }
                if ($gender == "") {
                    echo "<font color='red'>Hãy chọn giới tính.</font> </br>"; 
                }
                if ($department == "") {
                    echo "<font color='red'>Hãy chọn phân khoa.</font> </br> "; 
                }
                if ($date == "") {
                    echo "<font color='red'>Hãy nhập ngày sinh.</font> </br>"; 
                }
                if ($date != "") {
                    $date_arr  = explode('/', $date);
                    if (count($date_arr) != 3 || !checkdate($date_arr[1], $date_arr[0], $date_arr[2])) {
                        echo "<font color='red'>Hãy nhập ngày sinh đúng định dạng.</font> </br>";
                    }
                }
            }
              function test_input($data) {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
              }
          echo "
		        <form action='register.php' method='post'>
			        <div>
				        <label for = 'pwd' class='label' left=100%>Họ và tên<font color=red>*</font></label>
				        <input type='text' class='input' name='name' > <br>
				        <label for='pwd' class='label'>Giới tính<font color=red>*</font></label> " ;
					    for ($i = 0; $i < count($gender_arr); $i++) {
                            echo "
                                <input type='radio' class='gender' name='gender'> $gender_arr[$i] 
                            " ;
                        } 
                    echo "
                        <br>
                        <label for='pwd' class='label'>Phân khoa<font color=red>*</font></label> 
                        <select class= 'select' name='department'>
                            <option></option>"; 
                            foreach($departments as $department){
                                echo "<option>
                                $department
                                </option>";
                            }
                    echo "
                        </select> <br>
                        <label for='pwd' class='label'>Ngày sinh<font color=red>*</font></label>
                        <input placeholder='dd/mm/yyyy' type='text' class='date' name='date'/>  <br>
                        <label for='pwd' class='label'>Địa chỉ</label>
                        <input type='text' class='input' name='address' > <br>
				        <button type='submit' class='button' name='dangnhap'>Đăng ký</button>
			        </div>
		        </form>
            </fieldset>
	        </body>
        </html> " ;
?>